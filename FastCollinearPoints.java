import java.util.Comparator;
import java.util.Arrays;

import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

public class FastCollinearPoints {

    private LineSegment[] segments;
    private int numberOfSegments = 0;
    private boolean[] xIndices, yIndices;
    private int lineSegmentCounter;

    // finds all line segments containing 4 or more points
    public FastCollinearPoints(Point[] points){
        if(points == null) throw new IllegalArgumentException("Points array can't be null");
        if(hasDuplicatesOrNulls(points)) throw new IllegalArgumentException("Points array can't have duplicates");
        Point[] pointsCopy;
        segments = new LineSegment[points.length];
        lineSegmentCounter = 0;
        Arrays.sort(points);
        for(int i = 0; i < points.length; i++){
            pointsCopy = Arrays.copyOf(points, points.length);

            //get current point
            Point currentPoint = points[i];

            //sort the other points by angle to this one
            Arrays.sort(pointsCopy, currentPoint.slopeOrder());

            StdOut.println("current point"+currentPoint);
            StdOut.println("##########");
            for(int j = 0; j < pointsCopy.length; j++){
                StdOut.println(currentPoint+" : "+pointsCopy[j] + " the slope is "+currentPoint.slopeTo(pointsCopy[j]));
            }

            StdOut.println("##########################################");
            StdOut.println();
            //count all occurrences of >3 streaks
            int streakCounter = 1;

            //fetch angle to compare against for first streak
            int currentPosition = 1;
            double streakSlope = currentPoint.slopeTo(pointsCopy[currentPosition]);
            Point firstPoint = currentPoint, lastPoint = currentPoint;
            System.out.println("==================================START STREAK==================================");
            System.out.println("first point "+firstPoint+" na lastPoint "+lastPoint+" zinafaa ziequal currentPoint "+currentPoint);

            while(currentPosition < pointsCopy.length){
                int streakIndex = currentPosition;
                while(streakIndex < pointsCopy.length && currentPoint.slopeTo(pointsCopy[streakIndex]) == streakSlope){
                    Point anotherPoint = pointsCopy[streakIndex];
                    firstPoint = (less(firstPoint, anotherPoint) ? firstPoint : anotherPoint);
                    lastPoint = (less(lastPoint, anotherPoint) ? anotherPoint : lastPoint);
                    System.out.println("after adding the "+streakCounter+" th element, "+anotherPoint+" first point "+firstPoint+" na lastPoint "+lastPoint);
                    System.out.println("na streak slope ni "+streakSlope);
                    System.out.println("point printen in next line should have streak slope "+streakSlope);
                    System.out.println("segment point"+ pointsCopy[currentPosition]+" with streak slope "+ currentPoint.slopeTo(pointsCopy[currentPosition]));
                    streakIndex++;
                    streakCounter++;
                }
                if(streakCounter > 3){
                    if(firstPoint.compareTo(currentPoint) == 0) {
                        System.out.println("==================================BREAK GOOD STREAK==================================");
                        addSegment(firstPoint, lastPoint);
                        lineSegmentCounter++;
                    } else {
                        System.out.println("==================================BREAK ALMOST GOOD STREAK==================================");
                    }
                }
//                    StdOut.println("streak slope haifanyi kazi: "+slopesArray[currentPosition]+" na point yake ni "+pointsCopy[currentPosition]+", "+currentPosition);

//                    StdOut.println("hii streak haikuwa longer than 2, lenght: "+streakCounter);
                currentPosition = currentPosition + (streakIndex - currentPosition);
                if (currentPosition >= pointsCopy.length) continue;
                streakSlope = currentPoint.slopeTo(pointsCopy[currentPosition]);
                streakCounter = 1;
                firstPoint = currentPoint;
                lastPoint = currentPoint;
                System.out.println("==================================START STREAK==================================");
                System.out.println("first point "+firstPoint+" na lastPoint "+lastPoint+" zinafaa ziequal currentPoint "+currentPoint);


            }
        }
    }

    // the number of line segments
    public int numberOfSegments(){
        return lineSegmentCounter;
    }

    // the line segments
    public LineSegment[] segments(){
        LineSegment[] segmentsCopy = new LineSegment[lineSegmentCounter];
        for(int i = 0; i < segmentsCopy.length; i++){
            segmentsCopy[i] = segments[i];
        }
        return segmentsCopy;
    }

    private static boolean less(Point v, Point w) {
        if (v == w) return false;   // optimization when reference equals
        return v.compareTo(w) < 0;
    }

    private void addSegment(Point firstPoint, Point lastPoint) {
        LineSegment lineSegment = new LineSegment(firstPoint, lastPoint);
        for(int i = 0; i < lineSegmentCounter; i++){

        }
        segments[lineSegmentCounter] = lineSegment;
    }

    private boolean hasDuplicatesOrNulls(Point[] points){
        Point [] copy = Arrays.copyOf(points, points.length);
        Arrays.sort(copy);
        for (int i = 0; i < copy.length - 1; i++){
            if(copy[i].compareTo(copy[i+1]) == 0 ) return true;
            if (copy[i] == null) return true;
        }
        if (copy[copy.length-1] == null) return true;
        return false;
    }

    public static void main(String[] args) {

        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }
        StdOut.println("These are the points in the file");
        for(int j = 0; j < points.length; j++){
            StdOut.print(points[j]+" ");
        }
        StdOut.println();
        FastCollinearPoints fastCollinearPoints = new FastCollinearPoints(points);
        StdOut.println("They form "+fastCollinearPoints.numberOfSegments()+" segments");
    }
}