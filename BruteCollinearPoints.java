import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class BruteCollinearPoints {
    
    private int firstPoint, lastPoint;
    private Point [] points;
    private Deque<LineSegment> lineSegments = new Deque<LineSegment>();
    
    private int ccw(Point a, Point b, Point c){
        double area2 = ((b.getX() - a.getX())*(c.getY() - a.getY())) - ((b.getY()-a.getY())*(c.getX()-a.getX()));
        if (area2 < 0) return -1; // clockwise
        else if (area2 > 0) return +1; // counter-clockwise
        else return 0; // collinear
    }
    
    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] points){
        this.points = points;
        int lastForLoopCounter = 0;
        for(int i = 0; i < points.length; i++){
            boolean found = false;
            for(int j = 0; j < points.length; j++){
                for(int k = 0; k < points.length; k++){
                    if(i == j || j == k || i == k) continue;
                    Deque<Point> currentStreak = new Deque<Point>();
                    if(ccw(points[i],points[j],points[k])!=0) continue;
                    for(int l = 0; l < points.length; l++){
                        if(l == j || l == k || i == l) continue;
                        if(ccw(points[j], points[k], points[l])==0 ){
                            found = true;
                            currentStreak.addFirst(points[i]);
                            currentStreak.addFirst(points[j]);
                            currentStreak.addFirst(points[k]);
                            currentStreak.addFirst(points[l]);
                            LineSegment lineSegment = new LineSegment(currentStreak.removeLast(), currentStreak.removeFirst());
                            lineSegments.addFirst(lineSegment);
                            currentStreak = new Deque<Point>();
                        }
                    }
                }
            }
        }
    }
    
    // the number of line segments
    public int numberOfSegments(){
        return lineSegments.size();
    }
    
    // the line segments
    public LineSegment[] segments(){
        Deque<LineSegment> lineSegmentsCopy = lineSegments;
        LineSegment[] segments = new LineSegment[lineSegments.size()];
        for(int i = 0; i < segments.length; i++){
            segments[i] = lineSegmentsCopy.removeFirst();
        }
        return segments;
    }
    
    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }
        
        
        
        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();
        
        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
        StdOut.println(collinear.numberOfSegments());
    }
}